let programs = {
    1 :  {
        "name" : "CARRERAS PROFESIONALES Y CURSOS LIBRES",
        "details" : "Carreras Pre grado, Carreras para personas que trabajan, cursos, programas"   
    },    
    2 :  {
        "name" : "POST GRADO",
        "details" : "Doctorados, Maestrías, diplomados"   
    },    
    3 :  {
        "name" : "IDIOMAS",
        "details" : "Programas de idiomas"   
    }    
};


let places = {
    1 : "LIMA",    
    2 : "PROVINCIA",    
};


let data = [
    {
        "item":"11",
        "file_name":"convenio_11.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_1.png",
        "institution" : "Universidad de Ciencias y Humanidades",
        "promotion" : {
            "title" : "Tarifas preferenciales",
            "details" : `
                <span>- Carreras profesionales Pre grado</span><br>
                <span>- Programas de informática</span><br>
                <span>- Cursos Libres</span><br>
            `
        },
        "valid_for" : "Colaboradores y familiares (padres, hijos, cónyuge y hermanos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Jhony Manuel Dominguez Salazar",
            "address" : `
                <span>528 0948 - anexo: 1290</span><br>
                <span>Celular: 977 338 646 - 951 336 401</span><br>
                <span>jdominguez@uch.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "5/23/2020",
    },
    {
        "item":"12",
        "file_name":"convenio_12.html",
        "place_id" : 1,
        "program_id" : 3,
        "logo_url" : "images/institucion_1.png",
        "institution" : "Universidad de Ciencias y Humanidades",
        "promotion" : {
            "title" : "Tarifas preferenciales (pensiones)",
            "details" : `
                <span>- Programas de idiomas: inglés y portugués.</span>
            `
        },
        "valid_for" : "Colaboradores y familiares (padres, hijos, cónyuge y hermanos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Jhony Manuel Dominguez Salazar",
            "address" : `
                <span>528 0948 - anexo: 1290</span><br>
                <span>Celular: 977 338 646 - 951 336 401</span><br>
                <span>jdominguez@uch.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "5/23/2020",
    },
    {
        "item":"13",
        "file_name":"convenio_13.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_9.png",
        "institution" : "Cibertec",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- Hasta 10% de descuento en Programas de extensión profesional</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Alejandrina Alvarez Rodriguez",
            "address" : `
                <span>611 4900 - anexo 8406</span><br>
                <span>Celular.:997 566 396</span><br>
                <span>alejandrina.alvarez@cibertec.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "6/11/2018",
    },
    {
        "item":"14",
        "file_name":"convenio_14.html",
        "place_id" : 2,
        "program_id" : 1,
        "logo_url" : "images/institucion_9.png",
        "institution" : "Cibertec",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- Hasta 10% de descuento en Programas de extensión profesional</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Trujillo y Arequipa",
        "contacts" : {
            "names" : "Alejandrina Alvarez Rodriguez",
            "address" : `
                <span>611 4900 - anexo 8406</span><br>
                <span>Celular.:997 566 396</span><br>
                <span>alejandrina.alvarez@cibertec.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "6/11/2018",
    },
    {
        "item":"15",
        "file_name":"convenio_15.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_12.png",
        "institution" : "Universidad del Pacífico",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- Hasta 14% de descuento en Maestrías</span><br>
                <span>- Hasta 16% de descuento en Programas de Educación Ejecutiva</span>
            `
        },
        "valid_for" : "Sólo colaboradores *Aplica sólo para estudiantes nuevos",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Karina Bardales Medina",
            "address" : `
                <span>219 0100 anexo 5379</span><br>
                <span>Celular: 971  445  972</span><br>
                <span>mk.bardalesm@up.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "5/19/2018",
    },
    {
        "item":"16",
        "file_name":"convenio_16.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_2.png",
        "institution" : "Avansys",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 20% de descuento en carreras profesionales técnicas (3 años) y carreras técnicas (2 años).</span>
            `
        },
        "valid_for" : "Colaboradores y familiares (hijos, cónyuge y hermanos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Liria Belleza Yrala",
            "address" : `
                <span>411 5888 - anexo: 621</span><br>
                <span>Celular: 977 507 958</span><br>
                <span>liria.belleza@avansys.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "5/25/2018",
    },
    {
        "item":"17",
        "file_name":"convenio_17.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_5.png",
        "institution" : "Universidad César Vallejo",
        "promotion" : {
            "title" : "Tarifas preferenciales",
            "details" : `
                <span>- Carreras profesionales de Pre Grado</span><br>
                <span>- Programa de Formación para Adultos (Sube)</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Máximo Rudt Sifuentes",
            "address" : `
                <span>202 4342 - anexo 2079</span><br>
                <span>Celular:943 833 574</span><br>
                <span>mrudt@ucv.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/9/2019",
    },
    {
        "item":"18",
        "file_name":"convenio_18.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_5.png",
        "institution" : "Universidad César Vallejo",
        "promotion" : {
            "title" : "Tarifas preferenciales",
            "details" : `
                <span>- Programas de Post Grado (Maestrías y Doctorados)</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Máximo Rudt Sifuentes",
            "address" : `
                <span>202 4342 - anexo 2079</span><br>
                <span>Celular:943 833 574</span><br>
                <span>mrudt@ucv.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/9/2019",
    },
    {
        "item":"19",
        "file_name":"convenio_19.html",
        "place_id" : 2,
        "program_id" : 1,
        "logo_url" : "images/institucion_5.png",
        "institution" : "Universidad César Vallejo",
        "promotion" : {
            "title" : "Tarifas preferenciales",
            "details" : `
                <span>- Carreras profesionales de Pre Grado</span><br>
                <span>- Programa de Formación para Adultos (Sube)</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Chiclayo, Chimbote, Huaraz, Moyobamba, Piura, Tarapoto, Trujillo",
        "contacts" : {
            "names" : "Máximo Rudt Sifuentes",
            "address" : `
                <span>202 4342 - anexo 2079</span><br>
                <span>Celular:943 833 574</span><br>
                <span>mrudt@ucv.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/9/2019",
    },
    {
        "item":"20",
        "file_name":"convenio_20.html",
        "place_id" : 2,
        "program_id" : 2,
        "logo_url" : "images/institucion_5.png",
        "institution" : "Universidad César Vallejo",
        "promotion" : {
            "title" : "Tarifas preferenciales",
            "details" : `
                <span>- Programas de Post Grado (Maestrías y Doctorados)</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Chiclayo, Chimbote, Huaraz, Moyobamba, Piura, Tarapoto, Trujillo",
        "contacts" : {
            "names" : "Máximo Rudt Sifuentes",
            "address" : `
                <span>202 4342 - anexo 2079</span><br>
                <span>Celular:943 833 574</span><br>
                <span>mrudt@ucv.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/9/2019",
    },
    {
        "item":"21",
        "file_name":"convenio_21.html",
        "place_id" : 1,
        "program_id" : 3,
        "logo_url" : "images/institucion_3.png",
        "institution" : "Wall Street",
        "promotion" : {
            "title" : "Tarifas preferenciales",
            "details" : `
                <span>- Tarifas preferenciales en membresías de 6, 9, 12, 15 y 18 meses.</span><br>
                <span>- 15% de descuento en el curso preparatorio TOEFL.</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Enrique Mogollón",
            "address" : `
                <span>Celular: 981 521 137</span><br>
                <span>enriquem.tello@wallstreet-peru.com</span><br>
            `
        },
        "year" : "2017",
        "expires" : "5/24/2019",
    },
    {
        "item":"22",
        "file_name":"convenio_22.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_4.png",
        "institution" : "Instituto CEPEBAN",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 25% de descuento en carreras profesionales técnicas (3 años) y cursos de extensión.</span><br>
                <span>- 10% de descuento en programas de especialización</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Fiorella Wiesse Asenjo",
            "address" : `
                <span>222 2567 - anexo 1200</span><br>
                <span>Celular: 995 406 287</span><br>
                <span>fwiesse@cepeban.edu.pe</span><br>
                <span>marketing.cepeban@gmail.com</span>
            `
        },
        "year" : "2017",
        "expires" : "6/8/2019",
    },
    {
        "item":"23",
        "file_name":"convenio_23.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_8.png",
        "institution" : "CERTUS (Ex IFB)",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- Carreras profesionales: Exoneración de matrícula sólo para el primer ciclo + 10% de descuento en cuotas del primer ciclo</span><br>
                <span>- Certificaciones: 10% de descuento en todas las cuotas</span>
            `
        },
        "valid_for" : "Colaboradores y familiares (cónyuge, hijos, padres y hermanos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Karin Mesías Meléndez",
            "address" : `
                <span>202 0901 - anexo 2543</span><br>
                <span>Celular: 997 522 025</span><br>
                <span>kmesias@certus.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "6/18/2018",
    },
    {
        "item":"24",
        "file_name":"convenio_24.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_8.png",
        "institution" : "CERTUS (Ex IFB)",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- Carreras profesionales: Exoneración de matrícula sólo para el primer ciclo + 10% de descuento en cuotas del primer ciclo</span><br>
                <span>- Certificaciones: 10% de descuento en todas las cuotas</span>
            `
        },
        "valid_for" : "Colaboradores y familiares (cónyuge, hijos, padres y hermanos).",
        "sedes" : "Arequipa y Chiclayo",
        "contacts" : {
            "names" : "Karin Mesías Meléndez",
            "address" : `
                <span>202 0901 - anexo 2543</span><br>
                <span>Celular: 997 522 025</span><br>
                <span>kmesias@certus.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "6/18/2018",
    },
    {
        "item":"25",
        "file_name":"convenio_25.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_15.png",
        "institution" : "Instituto Tecnológico de Monterrey Perú",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- Hasta 9% de descuento en programas MBA (formato presencial)</span><br>
                <span>- Hasta 18% de descuento en programas Internacionales, certificaciones, cursos, talleres y seminarios</span>
            `
        },
        "valid_for" : "Sólo colaboradores",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Daniel Perez Torres",
            "address" : `
                <span>708 0400 anexo 113</span>
                <span>Celular: 943843787</span>
                <span>daniel.perez@invitados.itesm.mx</span>
            `
        },
        "year" : "2017",
        "expires" : "Se renueva de manera automática",
    },
    {
        "item":"26",
        "file_name":"convenio_26.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_13.png",
        "institution" : "Universidad Privada del Norte",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 10 % de descuento en las pensiones del programa Working Adult</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "José Nolberto Villalobos",
            "address" : `
                <span>604 4700 - anexo 3640</span><br>
                <span>Celular: 941 145 630</span><br>
                <span>jose.nolberto@upn.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/29/2019",
    },
    {
        "item":"27",
        "file_name":"convenio_27.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_13.png",
        "institution" : "Universidad Privada del Norte",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 10 % de descuento en el programa de MBA</span>
                <span>- 8% descuento en diplomados</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "José Nolberto Villalobos",
            "address" : `
                <span>604 4700 - anexo 3640</span><br>
                <span>Celular: 941 145 630</span><br>
                <span>jose.nolberto@upn.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/29/2019",
    },
    {
        "item":"28",
        "file_name":"convenio_28.html",
        "place_id" : 2,
        "program_id" : 1,
        "logo_url" : "images/institucion_13.png",
        "institution" : "Universidad Privada del Norte",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 10 % de descuento en las pensiones  del programa Working Adult</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Trujillo y Cajamarca",
        "contacts" : {
            "names" : "José Nolberto Villalobos",
            "address" : `
                <span>604 4700 - anexo 3640</span><br>
                <span>Celular: 941 145 630</span><br>
                <span>jose.nolberto@upn.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/29/2019",
    },
    {
        "item":"29",
        "file_name":"convenio_29.html",
        "place_id" : 2,
        "program_id" : 2,
        "logo_url" : "images/institucion_13.png",
        "institution" : "Universidad Privada del Norte",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 10 % de descuento en el programa de MBA</span><br>
                <span>- 8% descuento en diplomados</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Trujillo y Cajamarca",
        "contacts" : {
            "names" : "José Nolberto Villalobos",
            "address" : `
                <span>604 4700 - anexo 3640</span><br>
                <span>Celular: 941 145 630</span><br>
                <span>jose.nolberto@upn.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/29/2019",
    },
    {
        "item":"30",
        "file_name":"convenio_30.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_22.png",
        "institution" : "Universidad de Ingeniería y Tecnología",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 15% de descuento en preventa de programas de Educación Ejecutiva según las fechas por programa.</span>
                <span>- 5% de descuento en programas de Educación Ejecutiva.</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Lizbeth Herrera Melga",
            "address" : `
                <span>230 5000 - anexo 1034</span><br>
                <span>Celular: 997 373 649</span><br>
                <span>lherrera@utec.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/23/2020",
    },
    {
        "item":"31",
        "file_name":"convenio_31.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_24.png",
        "institution" : "Escuela de Postgrado USIL",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 15% de descuento en:</span> <br>
                <span>* Executive MBA</span><br>
                <span>* MBA Internacional (UQAM - Canadá)</span> <br>
                <span>* Maestría en Ciencias Empresariales/ Dirección de Marketing y Gestión Comercial (ESIC - ESPAÑA)</span> <br>
                <span>* Programas de Alta Especialización en Coaching</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge e hijos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Rocio León Garavito",
            "address" : `
                <span>518 3333 – anexo 1109</span><br>
                <span>Celular: 994 187 758</span><br>
                <span>rleon@usil.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/11/2018",
    },
    {
        "item":"32",
        "file_name":"convenio_32.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_23.png",
        "institution" : "Universidad Continental",
        "promotion" : {
            "title" : "Descuento sobre tarifa en la modalidad presencial, a distancia, gente que trabaja",
            "details" : `
                <span>- 50% de descuento en la admisión (todas las modalidades).</span><br>
                <span>- 50% de descuento en la matrícula (modalidad presencial).</span><br>
                <span>- 100% de descuento en la matrícula (modalidad gente que trabaja).</span><br>
                <span>- 10% de descuento en las mensualidades (modalidad a distancia).</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge, hijos, padres, hermanos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Michael Carpio Valencia / Ivana Novak",
            "address" : `
                <span>213 2760 – anexo 8007 - 6010</span><br>
                <span>Celular: 971  246  154</span><br>
                <span>mcarpiov@continental.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/31/2018",
    },
    {
        "item":"33",
        "file_name":"convenio_33.html",
        "place_id" : 2,
        "program_id" : 1,
        "logo_url" : "images/institucion_23.png",
        "institution" : "Universidad Continental",
        "promotion" : {
            "title" : "Descuento sobre tarifa en la modalidad presencial, a distancia, gente que trabaja",
            "details" : `
                <span>- 50% de descuento en la admisión (todas las modalidades).</span><br> 
                <span>- 50% de descuento en la matrícula (modalidad presencial).</span><br>
                <span>- 100% de descuento en la matrícula (modalidad gente que trabaja).</span><br>
                <span>- 10% de descuento en las mensualidades (modalidad a distancia).</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge, hijos, padres, hermanos).",
        "sedes" : "Arequipa, Cusco y Huancayo",
        "contacts" : {
            "names" : "Michael Carpio Valencia / Ivana Novak",
            "address" : `
                <span>213 2760 – anexo 8007 - 6010</span><br>
                <span>Celular: 971  246  154</span><br>
                <span>mcarpiov@continental.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/31/2018",
    },
    {
        "item":"34",
        "file_name":"convenio_34.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_23.png",
        "institution" : "Universidad Continental",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 10% de descuento en todos los programas (doctorados, maestrías, diplomados y formación continua).</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge, hijos, padres, hermanos).",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Michael Carpio Valencia / Ivana Novak",
            "address" : `
                <span>213 2760 – anexo 8007 - 6010</span><br>
                <span>Celular: 971  246  154</span><br>
                <span>mcarpiov@continental.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/31/2018",
    },
    {
        "item":"35",
        "file_name":"convenio_35.html",
        "place_id" : 2,
        "program_id" : 2,
        "logo_url" : "images/institucion_23.png",
        "institution" : "Universidad Continental",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 10% de descuento en todos los programas (doctorados, maestrías, diplomados y formación continua).</span>
            `
        },
        "valid_for" : "Colaboradores y familiares directos (cónyuge, hijos, padres, hermanos).",
        "sedes" : "Arequipa, Cusco y Huancayo",
        "contacts" : {
            "names" : "Michael Carpio Valencia / Ivana Novak",
            "address" : `
                <span>213 2760 – anexo 8007 - 6010</span><br>
                <span>Celular: 971  246  154</span><br>
                <span>mcarpiov@continental.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "7/31/2018",
    },
    {
        "item":"36",
        "file_name":"convenio_36.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_27.png",
        "institution" : "Universidad Peruana de Ciencias Aplicadas",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 10% de descuento en los programas de postgrado.</span>
            `
        },
        "valid_for" : "Sólo colaboradores",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Luisa Fernanda Gutierrez Pariona",
            "address" : `
                <span>Celular: 966 321-90</span><br>
                <span>luisa.gutierrez@upc.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "",
    },
    {
        "item":"37",
        "file_name":"convenio_37.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_27.png",
        "institution" : "Universidad Peruana de Ciencias Aplicadas",
        "promotion" : {
            "title" : "Tarifas preferenciales",
            "details" : `
                <span>- Programa de la División de Estudios Profesionales para Ejecutivos (EPE).</span>
            `
        },
        "valid_for" : "Sólo colaboradores",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "André Segouin Marquez",
            "address" : `
                <span>313 3333 - anexo 1356</span><br>
                <span>Celular: 941 409 681</span><br>
                <span>andre.segouin@upc.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "",
    },
    {
        "item":"38",
        "file_name":"convenio_38.html",
        "place_id" : 1,
        "program_id" : 2,
        "logo_url" : "images/institucion_26.png",
        "institution" : "Universidad de Lima",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- Del 5 al 30% de descuento (de acuerdo al n° de matriculados) en Maestrías:</span> <br>
                <span>* Dirección estratégica contenidos</span> <br>
                <span>* Tributación y política fiscal</span> <br>
                <span>* MBA</span> <br>
                <span>* Dirección de operaciones y proyectos</span> <br>
                <span>* Derecho empresarial</span> 
            `
        },
        "valid_for" : "Sólo colaboradores",
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Karina Molina Neyra",
            "address" : `
                <span> 317 2900 - anexo 36604 </span> <br>
                <span> Celular: 945 091 338 </span>  <br>
                <span> kmolina@ulima.edu.pe </span>
            `
        },
        "year" : "2017",
        "expires" : "",
    },
    {
        "item":"39",
        "file_name":"convenio_39.html",
        "place_id" : 1,
        "program_id" : 1,
        "logo_url" : "images/institucion_26.png",
        "institution" : "Zegel IPAE",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 15% de descuento sobre el costo total de las pensiones de los Programas Presenciales (no incluye matrícula).</span>
            `
        },
        "valid_for" : `
            <span>Colaboradores y familiares directos (cónyuge e hijos).</span> <br>
            <span>* En el caso de colaboradores solteros aplica a padres y hermanos.</span>
        `,
        "sedes" : "Lima",
        "contacts" : {
            "names" : "Karin Bazán",
            "address" : `
                <span>417 4000 - anexo 1354</span><br>
                <span>Celular: 953 892 259</span><br>
                <span>kbazan@zegelipae.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "9/4/2018",
    },
    {
        "item":"40",
        "file_name":"convenio_40.html",
        "place_id" : 2,
        "program_id" : 1,
        "logo_url" : "images/institucion_26.png",
        "institution" : "Zegel IPAE",
        "promotion" : {
            "title" : "Descuento sobre tarifa",
            "details" : `
                <span>- 15% de descuento sobre el costo total de las pensiones de los Programas Presenciales (no incluye matrícula).</span>
            `
        },
        "valid_for" : `
            <span>Colaboradores y familiares directos (cónyuge e hijos).</span><br>
            <span>* En el caso de colaboradores solteros aplica a padres y hermanos.</span>
        `,
        "sedes" : "Chiclayo, Piura, Ica, Iquitos",
        "contacts" : {
            "names" : "Karin Bazán",
            "address" : `
                <span>417 4000 - anexo 1354</span><br>
                <span>Celular: 953 892 259</span><br>
                <span>kbazan@zegelipae.edu.pe</span>
            `
        },
        "year" : "2017",
        "expires" : "9/4/2018",
    },
];



console.log(JSON.stringify(data));
