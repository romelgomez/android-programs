var handlebars  = require('handlebars');
var fs          = require('fs');
var del         = require('del');
var mkdirp      = require('mkdirp');
var getDirName  = require('path').dirname;
var Q = require('q');
var path = require('path');
var ncp = require('ncp').ncp;
ncp.limit = 16;



var src = {
    convenios : {
        data:       'src/convenios/data.json',
        template:   'src/convenios/template.html',
        fonts:      'src/convenios/fuentes',
        images:     'src/convenios/images',
        styles:     'src/convenios/styles'
    }
};
  
var output = {
    styles      : 'dist/styles',
    images      : 'dist/images',
    fonts       : 'dist/fuentes',
    json_files  : 'dist/json_files',
    dist        : 'dist'
};


function writeFile(path, contents, cb) {
    mkdirp(getDirName(path), function (err) {
        if (err) return cb(err);

        fs.writeFile(path, contents, cb);
    });
}
  
function readFile(fileName){
    var deferred = Q.defer();
  
    fs.readFile( path.join(process.cwd(), fileName), function (error, source) {
        if (error) {
            deferred.reject(error);
        }else{
            deferred.resolve(source);
        }
    });
  
    return deferred.promise;
}

function cleanUp () {

    del(['dist'])
        .then(paths => {
            console.log('Deleted files and folders:\n', paths.join('\n'));
        });

}

function merge ( __data, __template) {

    return Q.all([
            readFile(__data),
            readFile(__template)
        ])
        .then( promises => {

            let data        = JSON.parse( promises[0] ); // data
            let template    = handlebars.compile( promises[1].toString() ); // template

            __items = data.map( (item, index) => {

                return Q.when(function(){

                    data[index].html = template(item);
                    
                    return data[index];
                    
                }());

            });

            return (Q.all(__items));

        });

}

merge( src.convenios.data , src.convenios.template )
    .then( result => {

        cleanUp();

        //  lima 0 / provincia 1
        let __json = [
            {
                "data": [
                    {
                        "id":1,
                        "data" : []
                    },
                    {
                        "id":2,
                        "data" : []
                    },
                    {
                        "id":3,
                        "data" : []
                    }
                ]
            },
            {
                "data": [
                    {
                        "id":1,
                        "data" : []
                    },
                    {
                        "id":2,
                        "data" : []
                    },
                    {
                        "id":3,
                        "data" : []
                    }
                ]
            },
        ];

        // save convenio files | generate json data
        result.map( convenio => {

            writeFile(output.dist + '/' + convenio.file_name, convenio.html, function(err) {
                if(err) {
                    return console.log(err);
                }   
                // console.log("The file was saved!");
            });


            // Generate the JSON
            __json[convenio.place_id-1]
                .data[convenio.program_id-1]
                .data.push({
                    "id": __json[convenio.place_id-1].data[convenio.program_id-1].data.length + 1,
                    "name": convenio.institution,
                    "img": convenio.logo_url.replace('images/',''),
                    "info": "somosoh/mobile/conveniosEducativos/detalle/" + convenio.file_name
                });

        });


        // save json files.
        __json.map( (json_data, index)  => {

            file_name = index == 0 ? 'convenios_lima.json' : 'convenios_provincia.json';

            writeFile(output.json_files + '/' + file_name, JSON.stringify(json_data), function(err) {
                if(err) {
                    return console.log(err);
                }   
                // console.log("The file was saved!");
            });

        });

        // move styles and fonts
        ncp(src.convenios.fonts, output.fonts, function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('fonts done!');
        });

        ncp(src.convenios.styles, output.styles, function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('styles done!');
        });
        
        ncp(src.convenios.images, output.images, function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('images done!');
        });
        


    });




